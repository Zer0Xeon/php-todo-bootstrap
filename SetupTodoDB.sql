CREATE DATABASE todoapp;
USE todoapp;
CREATE TABLE todos (id integer PRIMARY KEY AUTO_INCREMENT, description text NOT NULL, completed boolean NOT NULL);
INSERT INTO todos (decription,completed) VALUES('If you see this the setup script worked. Now delete me or complete me',false);