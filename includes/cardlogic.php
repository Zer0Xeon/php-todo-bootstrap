<?php foreach($sth as $row):?>
<div class="card" style="width: 72rem;">
    <div class="card-body">
        <h5 class="card-title">TODO No. <?= htmlspecialchars($row['id']) ?></h5>
        <h6 class="card-subtitle mb-2 text-muted">
            <?php if($row['completed']):?>
            <i class="fas fa-check" style="font-size: 25px;"></i> Done
            <?php else:?>
            <i class="fas fa-clock" style="font-size: 25px;"></i> Todo
            <?php endif;?>
        </h6>
        <p class="card-text"><?=htmlspecialchars($row['description'])?></p>
        <div class="tpbutton btn-toolbar text-center">
            <form method="POST">
                <button class="btn btn-danger" type="submit" name="delete" value="true">Delete</button>
            </form>
            <form method="POST">
                <?php if($row['completed']):?>
                <button class="btn btn-warning" type="submit" name="uncomplete" value="true">Uncomplete</button>
                <?php else:?>
                <button class="btn btn-success" type="submit" name="complete" value="true">Complete</button>
                <?php endif;?>
                <input type="hidden" name="id" value="<?= htmlspecialchars($row['id']) ?>">
        </div>
    </div>
</div>
<?php endforeach;?>